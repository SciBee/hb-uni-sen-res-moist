#!/bin/tclsh

catch {
  set input $env(QUERY_STRING)
  set pairs [split $input &]

  foreach pair $pairs {
    if {$pair == "cmd=download"} {
      set cmd "download"
      break
    }
  }
}

set checkURL "https://bitbucket.org/SciBee/hb-uni-sen-res-moist/raw/ce13a853dc067effa210fe7a77943078861e8322/Addon/HB-UNI-Sen-RES-MOIST-addon-src/addon/VERSION"
set downloadURL "https://bitbucket.org/SciBee/hb-uni-sen-res-moist/raw/ce13a853dc067effa210fe7a77943078861e8322/HB-UNI-Sen-RES-MOIST-addonV2_1.tgz"

if { [info exists cmd ] && $cmd == "download"} {
  puts -nonewline "Content-Type: text/html; charset=utf-8\r\n\r\n"
  puts -nonewline "<html><meta http-equiv='refresh' content='0; url=$downloadURL' /><body></body></html>"
} else {
  catch {
    set newversion [ exec /usr/bin/wget -qO- --no-check-certificate $checkURL ]
  }
  if { [info exists newversion] } {
    puts $newversion
  } else {
    puts "n/a"
  }
}
