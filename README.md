# HB-UNI-Sen-RES-MOIST

Danke an Jérôme Pech, der [hier](https://www.facebook.com/esp8266hm) mit seiner großartigen Arbeit die Grundlagen
für diesen Sensor gelegt hat.

## Funk "resistiver Bodenfeuchtesensor" für die Integration in HomeMatic

## Verdrahtung

![wiring](Images/wiring.png)

## benötigte Hardware
* 1x Arduino Pro Mini **ATmega328P (3.3V / 8MHz)**
* 1x CC1101 Funkmodul **(868 MHz)**
* 1x FTDI Adapter (wird nur zum Flashen benötigt)
* 1x Taster (beliebig... irgendwas, das beim Draufdrücken schließt :smiley:)
* 1x LED 
* 1x Widerstand 330 Ohm (R6)
* 2x Widerstand 56k (R4, R5)
* 2x Widerstand 22k (R2, R3)
* 2x Kondensator 10nF (C1, C2)
* 2x Nirosta Speichen (normale Edelstahl Fahrradspeichen oder auch die Edelstahl Versteifung aus einem alten Scheibenwischer)
  Sie werden an "Jumper" angeschlossen und in die Erde/Seramis gesteckt. Die SpeichenlÃ¤nge orientiert sich an der Topfgröße.
* 1x Doppelte Lüsterklemme. Nirosta Speichen können nicht gelötet werden. Man steckt sie zur Verdrahtung einfach in eine Lüsterklemme.  
* Draht, um die Komponenten zu verbinden

Um die Batterielebensdauer zu erhöhen, ist es unbedingt notwendig, die grüne LED vom Arduino Pro Mini mit einem kleinen Schraubendreher oder Messer von der Platine zu entfernen!
Außerdem sollte der LDO Regler und ggf. die LED zu D13 (abhängig von der Arduino Ausführung) entfernt werden.

Es sind 2x AA(A)-Batterien (oder 3x 1.2V Akkus) notwendig.

## Code flashen
- [AskSinPP Library](https://github.com/pa-pa/AskSinPP) in der Arduino IDE installieren
  - Achtung: Die Lib benötigt selbst auch noch weitere Bibliotheken, siehe [README](https://github.com/pa-pa/AskSinPP#required-additional-arduino-libraries).
- [Projekt-Datei](https://bitbucket.org/SciBee/hb-uni-sen-res-moist/src/master/HB-UNI-Sen-RES-MOIST_V2.ino) herunterladen.
- Arduino IDE öffnen
  - Heruntergeladene Projekt-Datei öffnen
  - Werkzeuge
    - Board: Arduino Pro or Pro Mini
    - Prozessor: ATmega328P (3.3V 8MHz) 
    - Port: entsprechend FTDI Adapter
einstellen
- Menü "Sketch" -> "Hochladen" auswählen.

## Addon installieren
In der CCU2 (oder RaspberryMatic) muss vor dem Anlernen noch ein Addon installiert werden.
Dieses kann [hier](https://bitbucket.org/SciBee/hb-uni-sen-res-moist/src/master/Addon/HB-UNI-Sen-RES-MOIST-addon.tgz) heruntergeladen werden.

_Hinweis: Die Datei darf nicht entpackt werden!_

Über "Einstellungen"->"Systemsteuerung"->"Zusatzsoftware" wählt man die Datei aus und klickt auf "Installieren".
Die CCU2 startet automatisch neu.

**Achtung: Nachdem das System wieder hochgefahren ist, muss noch einmal ein Neustart erfolgen!**

Nun ist das Addon einsatzbereit.

## Gerät anlernen
Wenn alles korrekt verkabelt und das Addon installiert ist, kann das GerÃ¤t angelernt werden.
Über den Button "Gerät anlernen" in der WebUI öffnet sich der Anlerndialog.
Button "HM Gerät anlernen" startet den Anlernmodus.
Nun ist der Taster (an Pin D8) kurz zu drücken.
Die LED leuchtet für einen Moment.
Anschließend ist das Gerät im Posteingang zu finden.
Dort auf "Fertig" geklickt, wird es nun in der Geräteübersicht aufgeführt.

![addon](Images/ccu_geraete.png)

## Kalibrierung
Zur Kalibrierung steckt man die Speichen mit angeschlossener Schaltung in z.B. den Blumentopf, der überwacht werden soll.
Nun muss man abwarten, bis die Pflanze gegossen werden sollte. Das weiß man aus der bisherigen Erfahrung.
Den Wert, der vom Sensor zu diesem Zeitpunkt gemessen wurde (z.B. 22%) trägt man in der Homematic Einstellung zum Gerät unter "Minimale_Feuchte" ein.
Dann gießt man die Pflanze bis zur Sättigung und wartet einige Stunden, bis sich die Anzeige des Sensors nicht mehr nennenswert ändert.
Die Feuchte muss sich erst verteilen, die Erde etwas quellen...
Diesen Wert trägt man im Einstellungsfenster beim Wert für "Maximale_Feuchte" ein.

_Der Wert im Trockenen muss höher sein als im Nassen!_

![addon](Images/settings.png)

Nach "OK" in der WebUI zum Übertragen und Speichern der Werte nun den Konfigurationstaster des Sensors drücken.

## Einstellungen
- Gerät:
  - Low Bat Schwelle
	 ~2.2V
  - Das Sendeintervall kann zwischen 1 und 1440 Minuten eingestellt werden.
  - Minimale_Feuchte ist der gemessene Analogwert, wenn die Pflanze trocken ist.
  - Maximale_Feuchte ist der gemessene Analogwert, wenn die Pflanze gut gegossen ist.


## Wert anzeigen
Unter "Status und Bedienung" -> "Geräte" kann der Feuchtigkeitswert angezeigt werden.

Eine Verwendung in Programmen ist ebenfalls möglich.


## Beispielaufbau

![aufbau1](Images/aufbau1.png)
