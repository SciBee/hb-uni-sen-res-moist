//- -----------------------------------------------------------------------------------------------------------------------
// AskSin++
// 2016-10-31 papa Creative Commons - http://creativecommons.org/licenses/by-nc-sa/3.0/de/
//- -----------------------------------------------------------------------------------------------------------------------

// define this to read the device id, serial and device type from bootloader section
// #define USE_OTA_BOOTLOADER
// #define DEBUG //enable if you want to see more verbose output at the serial debug port

#define EI_NOTEXTERNAL
#include <EnableInterrupt.h>
#include <AskSinPP.h>
#include <LowPower.h>

#include <Register.h>
#include <MultiChannelDevice.h>

// Arduino Pro mini 8 Mhz
// Arduino pin for the config button
#define CONFIG_BUTTON_PIN 8

// Arduino pins for the sensor output
#define HIGHSIDE_PIN 6
#define LOWSIDE_PIN 5
// Arduino analog input pin for the spoke sensor input voltage A0 == PIN 14
#define SPOKE_PIN A0
// Arduino analog input pin for the battery sense input voltage A1 == PIN 15
#define BATTSENSE_PIN A1

// number of available peers per channel
#define PEERS_PER_CHANNEL 6


// all library classes are placed in the namespace 'as'
using namespace as;

// define all device properties
const struct DeviceInfo PROGMEM devinfo = {
  {0xF3, 0x12, 0x01},          // Device ID, increase by 1 for every new device of same type
  "SCIBEE0001",               // Device Serial, increase by 1 for every new device of same type
  {0xF3, 0x12},              // Device Model
  0x10,                       // Firmware Version
  as::DeviceType::THSensor,   // Device Type
  {0x01, 0x01}               // Info Bytes
};

/**
   Configure the used hardware
*/
typedef AvrSPI<10, 11, 12, 13> SPIType;
typedef Radio<SPIType, 2> RadioType;
typedef StatusLed<4> LedType;
typedef AskSin<LedType, BatterySensor, RadioType> BaseHal;
class Hal : public BaseHal {
  public:
    void init (const HMID& id) {
      BaseHal::init(id);
      battery.init(seconds2ticks(60UL * 60), sysclock); //battery measure once an hour
      battery.low(22);
      battery.critical(18);
    }

    bool runready () {
      return sysclock.runready() || BaseHal::runready();
    }
} hal;


DEFREGISTER(UReg0, MASTERID_REGS, DREG_BURSTRX, DREG_LOWBATLIMIT, 0x21, 0x22, 0x23, 0x24)
class UList0 : public RegList0<UReg0> {
  public:
    UList0 (uint16_t addr) : RegList0<UReg0>(addr) {}
    bool Sendeintervall (uint16_t value) const {
      return this->writeRegister(0x21, (value >> 8) & 0xff) && this->writeRegister(0x22, value & 0xff);
    #ifdef DEBUG
        Serial.print(F("Sendeinterval1: "));
        Serial.println(value);
    #endif 
    }
    uint16_t Sendeintervall () const {
      return (this->readRegister(0x21, 0) << 8) + this->readRegister(0x22, 0);
    }
    bool minmoisture (uint16_t value) const {
      return this->writeRegister(0x23, value);
    #ifdef DEBUG
        Serial.print(F("minmoisture1: "));
        Serial.println(value);
    #endif 
    }
    uint16_t minmoisture () const {
      return (this->readRegister(0x23, 0));
    }
    bool maxmoisture (uint16_t value) const {
      return this->writeRegister(0x24, value);
    #ifdef DEBUG
        Serial.print(F("maxmoisture1: "));
        Serial.println(value);
    #endif 
    }
    uint16_t maxmoisture () const {
      return (this->readRegister(0x24, 0));
    }

    void defaults () {
      clear();
      burstRx(false);
      lowBatLimit(22);
      Sendeintervall(30);
      minmoisture(0);
      maxmoisture(100);
    }
};

class WeatherEventMsg : public Message {
  public:
    void init(uint8_t msgcnt, uint8_t val, bool batlow) {
      Message::init(0x0c, msgcnt, 0x70, BCAST , batlow ? 0x80 : 0x00, 0x00);
      pload[0] = val;
    }
};

class WeatherChannel : public Channel<Hal, List1, EmptyList, List4, PEERS_PER_CHANNEL, UList0>, public Alarm {
    WeatherEventMsg msg;
    uint16_t      millis;

  public:
    WeatherChannel () : Channel(), Alarm(5), millis(0) {}
    virtual ~WeatherChannel () {}

    virtual void trigger (__attribute__ ((unused)) AlarmClock& clock) {
      uint8_t msgcnt = device().nextcount();
      tick = delay();
      sysclock.add(*this);

        //get the min/max values for scaling to 100% resolution
        int minmoisture = device().getList0().minmoisture();
        int maxmoisture = device().getList0().maxmoisture();
      
          digitalWrite(HIGHSIDE_PIN, HIGH);
          digitalWrite(LOWSIDE_PIN, LOW);
          _delay_ms(2);           
              unsigned int value1 = 0;
              for (uint8_t i = 0; i < 4; i++) {
                value1 += analogRead(BATTSENSE_PIN); //read output voltage of port which depends on draining battery voltage but is more precise than using the internal Bat voltage value for compensation
                _delay_ms(2);
              }
              value1 = value1 / 4;
              float outputvoltage = (value1 * ((((5600.0 + 2170.0) / 2170.0) * 1.1) / 1023.0)); //calculate output voltage on HIGHSIDE_PIN according to the resistor values in the measurement voltage devider

              unsigned int value2 = 0;
              for (uint8_t i = 0; i < 8; i++) {
                value2 += analogRead(SPOKE_PIN); // ((value2 * 100.0) / (value1)) is the 100% dry value. If the moisture increases the voltage on SPOKE_PIN decreases. Basically: moisture = 100% - voltage(SPOKE_PIN).
                _delay_ms(2);
              }
              value2 = value2 / 8;

          //reverse polarity to clear electrolytics build-up
          digitalWrite(HIGHSIDE_PIN, LOW);
          digitalWrite(LOWSIDE_PIN, HIGH);
          _delay_ms(28);
          //set both pins to low for further discharge until next reading
          digitalWrite(LOWSIDE_PIN, LOW);

        float moisture = (value2 * (((56000.0 + 22000.0) / 22000.0) * 1.1) / 1023.0 / (outputvoltage)) * 100.0;
    #ifdef DEBUG
        Serial.println(F("---------------------------------------"));
        Serial.print(F("moisture1: "));
        Serial.println(moisture);
    #endif

        moisture = 100 - moisture - minmoisture;
        moisture = ((moisture * 100) / maxmoisture);
    #ifdef DEBUG
        Serial.print(F("moisture2: "));
        Serial.println(moisture);
    #endif
        

      #ifdef DEBUG
        Serial.print(F("Adjusted minimum moisture: "));
        Serial.println(minmoisture);
        Serial.print(F("Adjusted maximum moisture: "));
        Serial.println(maxmoisture);
        Serial.print(F("Digital value from SPOKE PINs: "));
        Serial.println(value2);
        Serial.print(F("Calculated moisture percentage from SPOKE_PIN: "));
        Serial.println(moisture);
        Serial.print(F("Digital value from BATTSENSE PIN: "));
        Serial.println(value1);     
        Serial.print(F("Calculated voltage at BATTSENSE_PIN: "));
        Serial.print(outputvoltage);
        Serial.println(F("V"));
      #endif 

      msg.init(msgcnt, moisture, device().battery().low());

      device().sendPeerEvent(msg, *this);
    }

    uint32_t delay () {
      uint16_t _txMindelay = 30;
      _txMindelay = device().getList0().Sendeintervall();
      if (_txMindelay == 0) _txMindelay = 30;
      return seconds2ticks(_txMindelay * 60);
    }

    void configChanged() {
      //DPRINTLN("Config changed List1");
    }

    void setup(Device<Hal, UList0>* dev, uint8_t number, uint16_t addr) {
      Channel::setup(dev, number, addr);
      sysclock.add(*this);
    }

    uint8_t status () const {
      return 0;
    }

    uint8_t flags () const {
      return 0;
    }
};

class UType : public MultiChannelDevice<Hal, WeatherChannel, 1, UList0> {
  public:
    typedef MultiChannelDevice<Hal, WeatherChannel, 1, UList0> TSDevice;
    UType(const DeviceInfo& info, uint16_t addr) : TSDevice(info, addr) {}
    virtual ~UType () {}

    virtual void configChanged () {
      TSDevice::configChanged();
      DPRINTLN("Config Changed List0");
      DPRINT("LOW BAT Limit: ");
      DDECLN(this->getList0().lowBatLimit());
      DPRINT("Wake-On-Radio: ");
      DDECLN(this->getList0().burstRx());
      this->battery().low(this->getList0().lowBatLimit());
      DPRINT("Sendeintervall: "); DDECLN(this->getList0().Sendeintervall());
      DPRINT("Minimale Feuchte: "); DDECLN(this->getList0().minmoisture());
      DPRINT("Maximale Feuchte: "); DDECLN(this->getList0().maxmoisture());
    }
};

UType sdev(devinfo, 0x20);
ConfigButton<UType> cfgBtn(sdev);

void setup () {
  DINIT(57600, ASKSIN_PLUS_PLUS_IDENTIFIER);
  sdev.init(hal);
  buttonISR(cfgBtn, CONFIG_BUTTON_PIN);
      #ifdef DEBUG
        Serial.println(F("Debugging is enabled!"));
      #endif 
      analogReference(INTERNAL);
      pinMode(HIGHSIDE_PIN, OUTPUT);
      pinMode(LOWSIDE_PIN, OUTPUT);
      //explicitly disable internal pullups
      pinMode(SPOKE_PIN, INPUT);
      pinMode(BATTSENSE_PIN, INPUT);
  sdev.initDone();
}

void loop() {
  bool worked = hal.runready();
  bool poll = sdev.pollRadio();
  if ( worked == false && poll == false ) {
    if ( hal.battery.critical() ) {
      hal.activity.sleepForever(hal);
    }
    hal.activity.savePower<Sleep<>>(hal);
  }
}

